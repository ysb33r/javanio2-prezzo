package ysb33r.javanio2

import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

import java.nio.ByteBuffer
import java.nio.CharBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer
import java.nio.IntBuffer
import java.nio.LongBuffer
import java.nio.ShortBuffer
import java.nio.channels.SeekableByteChannel
import java.nio.file.FileSystem
import java.nio.file.FileSystems
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardOpenOption


class BasicNio2Spec extends Specification {
    @Rule final TemporaryFolder tempDir = new TemporaryFolder()

    File sourceDir
    void setup() {
        sourceDir = new File(tempDir.root,'files')
        sourceDir.mkdirs()
        new File(sourceDir,'1.txt').text = 'One'
        new File(sourceDir,'2.txt').text = 'Two'
    }

    def 'Load generic path'() {
        when:
        Path path = BasicNio2.basicNio2(sourceDir)

        then:
        path.toUri() == sourceDir.toURI()
    }

    def 'Load local FS path'() {
        when:
        Path path = BasicNio2.localFiles(sourceDir.absolutePath,'1.txt')

        then:
        path.toUri() == new File(sourceDir,'1.txt').toURI()
    }

    def 'Load Foo FS'() {
        when:
        FileSystem fs = BasicNio2.loadFileSystem()

        then:
        fs != null
    }

    def 'Read a file from FS'() {
        setup:
        FileSystem fs = BasicNio2.loadFileSystem()

        when:
        // tag::read-bytes[]
        Path src = Paths.get('foo://example/file.txt')
        SeekableByteChannel channel = fs.provider(). // <1>
                newByteChannel(src,[StandardOpenOption.READ].toSet()) //<2>
        ByteBuffer buffer = ByteBuffer.allocate(10) // <3>
        channel.read(buffer)
        // end::read-bytes[]

        then:
        buffer.get(5) as char == 'l'.toCharArray()[0]
    }
}