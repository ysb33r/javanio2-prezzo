package ysb33r.javanio2

import org.apache.commons.vfs2.FileObject
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification


class ApacheVfsSpec extends Specification {
    @Rule final TemporaryFolder tempDir = new TemporaryFolder()

    def 'Copy a file to RAM disk'() {
        setup:
        File sourceDir = new File(tempDir.root,'files')
        sourceDir.mkdirs()
        new File(sourceDir,'1.txt').text = 'One'
        new File(sourceDir,'2.txt').text = 'Two'

        when:
        FileObject  fo = ApacheVfs.copyFiles(sourceDir)

        then:
        fo.resolveFile('2.txt').exists()
    }

}