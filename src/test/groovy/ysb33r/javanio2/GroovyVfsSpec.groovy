package ysb33r.javanio2

import org.junit.Rule
import org.junit.rules.TemporaryFolder
import org.ysb33r.groovy.dsl.vfs.VFS
import spock.lang.Specification


class GroovyVfsSpec extends Specification {

    @Rule final TemporaryFolder tempDir = new TemporaryFolder()

    def 'Copy a file to RAM disk'() {
        setup:
        File sourceDir = new File(tempDir.root,'files')
        sourceDir.mkdirs()
        new File(sourceDir,'1.txt').text = 'One'
        new File(sourceDir,'2.txt').text = 'Two'

        when:
        // tag::copy-files[]
        VFS vfs = new VFS() // <1>

        vfs {
            mkdir 'ram://example' // <2>
            cp sourceDir, 'ram://example/', overwrite : true, // <3>
                    recursive: true
        }
        // end::copy-files[]

        then:
        vfs.exists('ram://example/files/1.txt')
    }
}