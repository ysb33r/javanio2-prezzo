package ysb33r.javanio2;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.*;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class BasicNio2 {

    public static Path localFiles(final String folderName,final String fileName) {
        // tag::local-files[]
        Path localFile = FileSystems.getDefault().
                getPath(folderName,fileName); // <1>
        // end::local-files[]
        return localFile;
    }

    public static Path basicNio2(File someFile) {
        URI someURI = someFile.toURI();

        // tag::basic-ops[]
        // Assume someURI == ram://example1/file1.txt
        Path path = Paths.get(someURI); // <1>
        // end::basic-ops[]

        return path;
    }

    public static FileSystem loadFileSystem() throws Exception {
        // tag::load-fs[]
        Map<String,Object> options = new LinkedHashMap<String,Object>();
        URI uri = new URI("foo://example");
        FileSystem fs = FileSystems.newFileSystem(uri,options); // <1>
        // end::load-fs[]
        return fs;
    }

    public static FileSystem safeLoadFileSystem() throws Exception {
        Map<String,Object> options = new LinkedHashMap<String,Object>();
        URI uri = new URI("foo://example");

        // tag::safe-load-fs[]
        FileSystem fs;
        try {
            fs = FileSystems.getFileSystem(uri);
        } catch (FileSystemNotFoundException e) {
            fs = FileSystems.newFileSystem(uri,options);
        }
        // end::safe-load-fs[]
        return fs;
    }

}
