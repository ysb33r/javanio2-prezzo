package ysb33r.javanio2.providers.foo;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.StandardOpenOption;

public class FooSeekableByteChannel implements SeekableByteChannel {

    @Override
    public int read(ByteBuffer dst) throws IOException {
        dst.putChar('H');
        dst.putChar('e');
        dst.putChar('l');
        dst.putChar('l');
        dst.putChar('o');
        return 5;
    }

    @Override
    public int write(ByteBuffer src) throws IOException {
        return 0;
    }

    @Override
    public long position() throws IOException {
        return 0;
    }

    @Override
    public SeekableByteChannel position(long newPosition) throws IOException {
        return null;
    }

    @Override
    public long size() throws IOException {
        return 5;
    }

    @Override
    public SeekableByteChannel truncate(long size) throws IOException {
        return null;
    }

    @Override
    public boolean isOpen() {
        return true;
    }

    @Override
    public void close() throws IOException {

    }
}
