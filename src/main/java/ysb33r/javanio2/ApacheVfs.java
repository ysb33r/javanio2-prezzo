package ysb33r.javanio2;


import org.apache.commons.vfs2.*;
import org.apache.commons.vfs2.FileObject;

import javax.tools.*;
import java.io.File;

public class ApacheVfs {
    static FileObject copyFiles(final File sourceDir) throws FileSystemException {
        // tag::copy-files[]
        FileSystemManager vfs = VFS.getManager(); // <1>

        FileObject src = vfs.resolveFile(sourceDir.toURI()); // <2>
        FileObject dest = vfs.resolveFile("ram://example"); // <3>
        dest.createFolder(); // <4>
        dest.copyFrom(src, new AllFileSelector() ); // <5>
        // end::copy-files[]

        return dest;
    }
}
