#include <fnctl.h>

int main() {
  // tag::read-bytes[]
  char[10] buf;
  int fd = open("/path/to/file.txt",O_RDONLY);
  read(fd,buf,10);
  // end::read-bytes[]
}