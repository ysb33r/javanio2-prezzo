== Implementing NIO FS

Minimum classes for a RAM filesystem

* `RamFileSystemProvider`
* `RamFileSystem`
* `RamFileStore`
* `RamFilePath`
* `RamSeekableByteChannel`
* Services file: `META-INF/services/java.nio.file.spi.FileSystemProvider`