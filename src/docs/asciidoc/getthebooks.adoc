=== Get Your Daily Gradle Dose

image::images/gradle-logo-pink.png[height="250",width="250"]

@DailyGradle

#gradleTip

=== Get the Books

image::images/front-cover-thumbnail-2.png[Book,506,428]

https://leanpub.com/b/idiomaticgradle

[NOTE.speaker]
--
Gradle plugins authored/contributed to: VFS, Asciidoctor, JRuby family (base, jar, war etc.), GnuMake, Doxygen, Bintray
11 own
5 co-author/maintainer in other organisations
--
