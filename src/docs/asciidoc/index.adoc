= Trials & Tribulations : Java NIO2
:author: Schalk W. Cronjé
:email: ysb33r@gmail.com
:twitter: @ysb33r
:docinfo1:
:revealjs_keyboard: true
:revealjs_overview: true
:revealjs_theme: ysb33r
:revealjs_controls: true
:revealjs_history: true
:revealjs_slideNumber : true
:revealjs_center: false
:icons: font

[NOTE.speaker]
--
Bienvinguda al dia dos de JBCNConf
--

include::about-me.adoc[]

include::getthebooks.adoc[]

include::about-this.adoc[]

include::the-goals.adoc[]

include::basic-apache-vfs.adoc[]

include::basic-groovy-vfs.adoc[]

include::nio2-concepts.adoc[]

include::basic-nio2.adoc[]

include::hard-questions.adoc[]

include::recommended-practices.adoc[]

include::implementation-start.adoc[]

include::thankyou.adoc[]


